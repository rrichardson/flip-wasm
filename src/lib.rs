extern crate cfg_if;
extern crate wasm_bindgen;

mod utils;

use cfg_if::cfg_if;
use std::fmt::Debug;
use wasm_bindgen::prelude::*;
use serde_derive::Serialize;
use rand::{ thread_rng, seq::SliceRandom };
use serde_wasm_bindgen;
use wasm_bindgen::{JsValue};

const INDEX:  &str = "^(;,;)^";

const GRAPHICS: [&str; 8] = [
    "(╯°Д°）╯︵/(.□ . \\)",
    "(˚Õ˚)ر ~~~~╚╩╩╝",
    "ヽ(ຈل͜ຈ)ﾉ︵ ┻━┻",
    "(ノಠ益ಠ)ノ彡┻━┻",
    "(╯°□°）╯︵ ┻━┻",
    "(┛◉Д◉)┛彡┻━┻",
    "┻━┻︵ \\(°□°)/ ︵ ┻━┻",
    "(┛ಠ_ಠ)┛彡┻━┻",
];

const IN_CHANNEL: &str = "in_channel";

/// This type will be part of the web service as a resource.
#[derive(Clone, Debug, Serialize)]
pub struct FlipResponse {
    response_type: &'static str,
    text: &'static str,
    attachments: Vec<String>,
}


cfg_if! {
    // When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
    // allocator.
    if #[cfg(feature = "wee_alloc")] {
        extern crate wee_alloc;
        #[global_allocator]
        static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
    }
}

#[wasm_bindgen]
pub fn flip() -> JsValue {
    let mut rng = thread_rng();
    GRAPHICS.choose(&mut rng).map(|v| {
        let res = FlipResponse { response_type: IN_CHANNEL, text: *v, attachments: Vec::new()};
        serde_wasm_bindgen::to_value(&res)
    }).unwrap().unwrap()
}

#[wasm_bindgen]
pub fn index() -> JsValue {
    let res = FlipResponse { response_type: IN_CHANNEL, text: INDEX, attachments: Vec::new()};
    serde_wasm_bindgen::to_value(&res).unwrap()
}

