addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

/**
 * Fetch and log a request
 * @param {Request} request
 */
async function handleRequest(request) {
    const { flip } = wasm_bindgen;
    await wasm_bindgen(wasm);
    const fl = JSON.stringify(flip());
    const resp = new Response(fl, {status: 200})
    resp.headers.set('Content-type', 'application/json')
    return resp;
}
